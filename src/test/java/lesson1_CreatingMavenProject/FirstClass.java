package lesson1_CreatingMavenProject;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class FirstClass {
    public static void main(String[] args) {

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");

        /**
         * 1- Bonigarcia'dan alınan WebDriverManeger uzerınden browser ıle ılgılı ayarları yapmamız gerekır.
         *    Orn: WebDriverManager.chromedriver().setup();
         *
         * 2- Selenium'dan aldıgımız WebDriver ile bir driver objesi olusturmamız gerekir.
         *    Orn: WebDriver driver = new ChromeDriver(chromeOptions);
         */

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver(chromeOptions);

        driver.get("https://www.amazon.com");


    }
}
