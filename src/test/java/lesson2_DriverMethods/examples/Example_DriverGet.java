package lesson2_DriverMethods.examples;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;

public class Example_DriverGet {

    /**
     * 1- Amazon sayfasına gidin. https://www.amazon.com/
     * 2- Sayfa baslıgını (title) yazdirin.
     * 3- Sayfa basliginin Amazon icerdigini test edin.
     * 4- Sayfa adresini (url) yazdırın.
     * 5- Sayfa url'sinin "amazon" icerdigini test edin.
     * 6- Sayfa handle degerini yazdırın.
     * 7- Sayfa HTML kodlarında "shopping" kelimesi gectigini test edin.
     * 8- Sayfayi kapatın
     */

    public static void main(String[] args) throws InterruptedException {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver(chromeOptions);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));

        // 1- Amazon sayfasına gidin. https://www.amazon.com/
        driver.navigate().to("https://www.amazon.com/");


        // 2- Sayfa baslıgını (title) yazdirin.
        String actualTitle = driver.getTitle();
        System.out.println("Sayfa Title: " + actualTitle);


        // 3- Sayfa basliginin Amazon icerdigini test edin.
        String expectedTitle = "Amazon";
        if (actualTitle.contains(expectedTitle)){
            System.out.println("Title testi PASSED");
        }else {
            System.out.println("Title testi FAİLED");
        }


        // 4- Sayfa adresini (url) yazdırın.
        String actualURL = driver.getCurrentUrl();
        System.out.println("Sayfanın URL : " + actualURL);


        // 5- Sayfa url'sinin "amazon" icerdigini test edin.
        String expectedURL = "amazon";
        if(actualURL.contains(expectedURL)){
            System.out.println("URL testi PASSED");
        }else {
            System.out.println("URL testi FAILED");
        }

        // 6- Sayfa handle degerini yazdırın.
        String valueOfHandle = driver.getWindowHandle();
        System.out.println("Sayfanın Handle Degeri : " + valueOfHandle);

        // 7- Sayfa HTML kodlarında "shopping" kelimesi gectigini test edin.
        String pageSource = driver.getPageSource();
        String expected = "shopping";
        if(pageSource.contains(expected)){
            System.out.println("HTML testi PASSED");
        }else {
            System.out.println("HTML testi FAILED");
        }

        // 8- Sayfayı kapatın
        Thread.sleep(2000);
        driver.quit();


    }
}
