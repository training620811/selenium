package lesson2_DriverMethods.examples;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;

public class Example1_DriverManage {


    /**
     * 1- Youtube sayfasına gıdelım.
     * 2- Sayfanın konumunu ve boyutlarını yazdırın
     * 3- Sayfayı sımge durumuna getitirin
     * 4- Simge durumunda 3 sanıye bekleyıp sayfayı maximize yapın
     * 5- Sayfanın konumunu ve boyutlarını maxımıze durumunda yazdırın
     * 6- Sayfayı fullscreen yapın
     * 7- Sayfanın konumunu ve boyutlarını fullscreen durumunda yazdırın
     * 8- Sayfayı kapatın
     */
    public static void main(String[] args) throws InterruptedException {

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver(chromeOptions);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));

        // 1- Youtube sayfasına gıdelım.
        driver.get("https://www.youtube.com");

        // 2- Sayfanın konumunu ve boyutlarını yazdırın
        System.out.println("Sayfanın konumu : " +driver.manage().window().getPosition());
        System.out.println("Sayfanın boyutu : " + driver.manage().window().getSize());

        // 3- Sayfayı sımge durumuna getitirin
        driver.manage().window().minimize();

        // 4- Simge durumunda 3 sanıye bekleyıp sayfayı maximize yapın
        Thread.sleep(3000);
        driver.manage().window().maximize();

        // 5- Sayfanın konumunu ve boyutlarını maxımıze durumunda yazdırın
        System.out.println("Maximize konum : " + driver.manage().window().getPosition());
        System.out.println("Maximize boyut : " + driver.manage().window().getSize());

        // 6- Sayfayı fullscreen yapın
        driver.manage().window().fullscreen();

        // 7- Sayfanın konumunu ve boyutlarını fullscreen durumunda yazdırın
        System.out.println("Fullscreen konum : " + driver.manage().window().getPosition());
        System.out.println("Fullscreen boyut : " + driver.manage().window().getSize());

        // 8- Sayfayı kapatın
        driver.close();


    }

}
