package lesson2_DriverMethods.examples;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;

public class Example_DriverNavigate {


    /**
     * 1-Amazon ana sayfasına gıdelım
     * 2-Youtube sayfasına gidelim
     * 3-Yenıden amazona donelım
     * 4-Tekrar youtube sayfasına gıdelım
     * 5-sayfayı Refresh yapalım
     * 6-sayfayı kapatalım.
     */
    public static void main(String[] args) throws InterruptedException {

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver(chromeOptions);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));

        // 1
        driver.get("https://www.amazon.com");
        Thread.sleep(2000);

        // 2
        driver.navigate().to("https://www.youtube.com");
        Thread.sleep(2000);

        // 3
        driver.navigate().back();
        Thread.sleep(2000);

        // 4
        driver.navigate().forward();
        Thread.sleep(2000);

        // 5
        driver.navigate().refresh();
        Thread.sleep(2000);

        // 6
        driver.close();

    }
}
