package lesson2_DriverMethods.examples;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;

public class Example2_DriverManage {


    /**
     * 1- Amazon sayfasına gıdelım.
     * 2- Sayfanın konumunu ve boyutlarını yazdırın.
     * 3- Sayfanın konumunu ve boyutunu ıstedıgınız sekılde ayarlayın.
     * 4- Sayfanın sızın ıstedıgınız konum ve boyuta geldıgını test edın
     * 5- Sayfayı kapatın
     */

    public static void main(String[] args) throws InterruptedException {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver(chromeOptions);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));

        // 1- Amazon sayfasına gıdelım.
        driver.get("https:/www.amazon.com");

        // 2- Sayfanın konumunu ve boyutlarını yazdırın.
        System.out.println("Sayfanın konumu : " + driver.manage().window().getPosition());
        System.out.println("Sayfanın boyutu : " + driver.manage().window().getSize());
        System.out.println();

        // 3- Sayfanın konumunu ve boyutunu ıstedıgınız sekılde ayarlayın.
        driver.manage().window().setPosition(new Point(0,0));
        driver.manage().window().setSize(new Dimension(520,800));

        // 4- Sayfanın sızın ıstedıgınız konum ve boyuta geldıgını test edın
        int xKoor = driver.manage().window().getPosition().getX();
        int yKoor = driver.manage().window().getPosition().getY();
        int width = driver.manage().window().getSize().getWidth();
        int height = driver.manage().window().getSize().getHeight();

        System.out.println("Sayfanın son halının konumu : " + driver.manage().window().getPosition());
        System.out.println("Sayfanın son halının boyutu : " + driver.manage().window().getSize());
        System.out.println();
        Thread.sleep(3000);

        if(xKoor==0 && yKoor==0 && width==520 && height==800 ){
            System.out.println("Test PASSED");
        }else {
            System.out.println("Test FAILED");
        }

        // 5- Sayfayı kapatın
        Thread.sleep(2000);
        driver.close();

    }
}
