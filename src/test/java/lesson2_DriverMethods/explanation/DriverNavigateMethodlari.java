package lesson2_DriverMethods.explanation;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class DriverNavigateMethodlari {
    public static void main(String[] args) {

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver(chromeOptions);

        // 1- driver.navigate().to("url"); --> istenen url'e gider
        //    driver.get("url") ile ayni islemi yapar ama forward ve back yapilmasina da imkan tanir.
        driver.get("https://www.amazon.com");
        driver.navigate().to("https://www.facebook.com");

        // 2- driver.navigate().back(); --> bir onceki sayfaya donus yapar.
        driver.navigate().back(); // ornegımızde amazon'a donus yapar.

        // 3- driver.navigate().forward(); --> back() ile geldigi sayfaya tekrar gider.
        driver.navigate().forward(); // ornegımızde facebook'a donus yapacaktır.

        // 4- driver.navigate().refresh(); --> icinde oldugu sayfayı yeniler.
        driver.navigate().refresh();

        // driver olusturuldugunda acılan sayfayı kapatmak yani icinde bulundugu "tek sayfayı" kapatmak istersek;
        driver.close();

        // driver calısırken bırden fazla tab veya window actıysa tumunu kapatmak ıstersek;
        // driver.quit();

    }
}
