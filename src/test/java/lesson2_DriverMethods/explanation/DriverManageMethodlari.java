package lesson2_DriverMethods.explanation;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;

public class DriverManageMethodlari {
    public static void main(String[] args) {

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver(chromeOptions);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));

        // 1- driver.manage().window() method'ları

        // 1A- driver.manage().window().getSize(); --> icinde oldugu sayfanın pixel olarak olculerını döndürür.
        System.out.println(driver.manage().window().getSize()); // (1051, 815) pixel
        System.out.println(driver.manage().window().getSize().height); // (1051)

        // 1B- driver.manage().window().getPosition(); -->  icinde oldugu sayfanın pixel olarak konumunu döndürür.
        System.out.println(driver.manage().window().getPosition()); // (9, 9)

        // 1C- driver.manage().window().setPosition(new Point(15,15));
        //     --> icinde oldugu sayfanın sol alt kosesını bızım yazacagımız pixel noktasına tasir.
        driver.manage().window().setPosition(new Point(12, 12));

        // 1D- driver.manage().window().setSize(new Dimension(900,600));
        //     --> icinde oldugu sayfanın sol alt kosesı sabıt olarak bızım yazacagımız olculere getırır.
        driver.manage().window().setSize(new Dimension(800, 500));

        // konumu ve boyutu yenıledıkten sonra tekrar yazdırırsak;
        System.out.println("yeni pencere olculeri : " + driver.manage().window().getSize());
        System.out.println("yeni pencere konumu : " + driver.manage().window().getPosition());

        // 1E- driver.manage().window().maximize(); --> icinde oldugu sayfayı maximize yapar.
        // 1F- driver.manage().window().fullscreen(); --> ixinde oldugu sayfayı fullscreen yapar.


        // farklarını gorebılmek ıcın method'ları kullanıp, baslangıc noktalarını ve boyutlarını yazdıralım..!!

        driver.manage().window().maximize();
        System.out.println("maximize konum : " + driver.manage().window().getPosition());
        System.out.println("maximize boyut : " + driver.manage().window().getSize());

        driver.manage().window().fullscreen();
        System.out.println("fullscreen konum : " + driver.manage().window().getPosition());
        System.out.println("fullscreen boyut : " + driver.manage().window().getSize());

        // 1G- driver.manage().window().minimize(); --> icinde oldugu sayfayı minimize yapar. Sımge durumunda kucultur.
        driver.manage().window().minimize();


        // 2- diver.manage().timeouts() method'lari

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
        /*      implicitlyWait : driver'a sayfanın yuklenmesı ve kullancagımız web elementlerın bulunması ıcın
                                 bekleyecegi maximum sureyı belırtır.
                                 driver bu sure ıcerısınde sayfa yuklenır/web element bulunursa
                                 beklemeden calismaya devam eder.
                                 Bu sure bıttıgı halde sayfa yuklenmemıs/web element bulunmamıssa
                                 exception vererek calısmayı durdurur.

               duration.ofSeconds(15) : Duration class'i Selenium-4 ile gelen zaman class'ıdır.
                                        Yani driver'a ne kadar bekleyecegını soyler
                                        Duration.ofSeconds(15) yerine milis,minute,hours da kullanılabılır.
         */


        /*
            NOT : Driver'in istedigimiz islemleri yaparken sorunla karsılasmaması ıcın
                  driver.manage() method'larindan
                  maximize() ve implicitlyWait() method'larinin her test'te kullanılması
                  FAYDALI OLACAKTIR.
         */




    }
}
