package lesson2_DriverMethods.explanation;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class DriverGetMethodlari {
    public static void main(String[] args) {

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver(chromeOptions);

        // 1 - driver.get("url"); --> yazdıgımız url'e gider. https, muhakkak yazılmalıdır.
        driver.get("https://www.amazon.com");


        // 2- driver.getTitle(); --> icinde oldugu sayfanin baslıgını "String" olarak dondurur. Degiskende tutulabılır.
        System.out.println("Sayfa title: " + driver.getTitle()); //Amazon.com Spend less. Smile more.


        // 3- driver.getCurrentUrl(); --> icinde oldugu sayfanın URL'ini "String" olarak dondurur. Degıskende tutulabılır.
        System.out.println("Sitenin URL'si :" +driver.getCurrentUrl()); // https://www.amazon.com/


        // 4- driver.getPageSource(); --> icinde oldugu sayfanin kaynak kodlarını "String" olarak dondurur. Degiskende tutulabılır.
        System.out.println("=======================================================================");
        System.out.println(driver.getPageSource()); // arka planda calısan sayfa kodlarını yazdırır.
        System.out.println("=======================================================================");


        // 5- driver.getWindowHandle(); --> icinde oldugu sayfanın UNIQUE hash kodunu döndürür. Tek bır sayfanın dondurur.
        System.out.println(driver.getWindowHandle()); // CDwindow-F4A35...... (her calısmada degısır, her sayafaya ait unıque'dir)


        // 6- driver.getWindowHandles(); --> driver calısırken acılan tum sayfaların UNIQUE hash kodunu dondurur.
        //                                   Tum tab ve sayfaların hash kodunu dondurur.
    }
}
