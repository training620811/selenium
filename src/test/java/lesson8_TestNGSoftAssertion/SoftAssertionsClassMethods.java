package lesson8_TestNGSoftAssertion;

public class SoftAssertionsClassMethods {

    /**
     * -Junıt'de Assert class ıle bırden fazla test yaptıgımızda bır tanesı FAILED olunca dıgerlerıne gecmeden testcase durdurulur
     * -TestNG'de
     *  1- SoftAssert class ı ıle aynı Junit'de yaptıgımız assertion'ları yapabılırız.
     *     Ilk failed den sonra dururuabılırız.
     *  2- Ya da Faıled'lerı toplu olarak vermesını ısteyebılırız.
     *     Boylece test case durdurmadan en son assert hatalarını toplu verir.
     *     Yani SoftAssert Class'ı ıle assertion yaptıgımızda faıled durumu ıle karsılasılsa bıle, assertAll() ile karsılasıncaya
     *     kadar calısmaya devam eder.
     *     assertAll() calıstıgında o test ıcerısınde softAssert ile yapılan tum assertion'ları kontrol eder, FAILED olan varse
     *     calısmayı durdurur ve hata mesajı yayınlar.
     *     FAILED yoksa kod calısmaya devam eder.
     *
     * -SoftAssert dogrulama (verification) olarak da bılınır.
     *  Test edin denildiginde Assert class'ını dusunuruz.
     *  Dogrulayın denıldıgınde SoftAssert Class'ını dusunuruz.
     *
     * -SoftAssertion methosları kullanmak ıcın
     *  1-SoftAssert objesı olusturulur.
     *    SoftAssert softAssert = new SoftAssert();
     *  2-Istedigimiz sayıda verification'ları yaparız
     *    softAssert.assertTrue();
     *    softAssert.assertFalse();
     *    softAssert.assertEqual(); .....
     *  3-SoftAssert'ın durumu raporlamasını ısterız
     *    softAssert.assertAll();
     *
     */
}
