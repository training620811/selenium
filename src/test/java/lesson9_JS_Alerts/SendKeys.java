package lesson9_JS_Alerts;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class SendKeys  {

    WebDriver driver;

    @BeforeMethod
    public void setup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @Test
    public void test() throws InterruptedException {

        driver.get("https://the-internet.herokuapp.com/javascript_alerts");

        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
        button.click();
        driver.switchTo().alert().sendKeys("Deneme");
        Thread.sleep(2000);
        driver.switchTo().alert().accept();

        WebElement resutl = driver.findElement(By.cssSelector("p[id='result']"));
        String expectedResult ="You entered: Deneme";
        String actualResult = resutl.getText();
        Assert.assertEquals(actualResult,expectedResult,"Result Test");
    }

    @AfterMethod
    public void tearDown(){
        driver.close();
    }
}
