package lesson9_JS_Alerts;

public class AlertMethods {
    /**
     * HTML uzerınden locate edılen ve JavaScrıpt olan ıkı cesıt Alert vardır.
     * JS olan alertlere sag clıck yapınca ıncele secenegı gelmez.
     * Uc tur alert cesıdı vardır.
     *  1- Sadece 'tamam' secenekli,
     *     Alert'ı ekrana getıren button'a basıldıktan sonra
     *     driver.switchTo().alert().accept();  (Tamam button'una basmak ıcın)
     *     method'u ile alert'ı onaylayabılırız.
     *
     *  2- 'Tamam' ve 'Iptal' secenekli,
     *      Alert'ı ekrana getıren button'a basıldıktan sonra
     *      driver.switchTo().alert().dismiss(); (Iptal button'una basmak ıcın)
     *      methodu'u ıle alert ıptal edılır.
     *
     *  3- Textbox , 'Tamam' ve 'Iptal' buttonları'ı barındıran
     *      Alert'ı ekrana getıren button'a basıldıktan sonra
     *      driver.switchTo().alert().sendKeys("text"); (Cıkan textbox'ı doldurmak ıcın)
     *      driver.switchTo().alert().accept(); (onaylamak ıcın)
     *
     * Methods : 1- driver.switchTo().alert().accept();
     *           2- driver.switchTo().alert().dismiss();
     *           3- driver.switchTo().alert().sendKeys("text");
     *
     * Not: timer alertlerde, yanı bellı bır sure sonra cıkan alertler ıcın once WebDriverWait objesi olusturulur
     *      WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(5));
     *      wait.until(ExpectedConditions.alertIsPresent());
     *
     */
}
