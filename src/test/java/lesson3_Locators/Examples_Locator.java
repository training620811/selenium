package lesson3_Locators;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;

public class Examples_Locator {
    public static void main(String[] args) throws InterruptedException {

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver(chromeOptions);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));

        // Id - https://demoqa.com/text-box sitesine git ve Full Name kutusune tıkla
        driver.get("https://demoqa.com/text-box");
        WebElement nameElement = driver.findElement(By.id("userName"));
        nameElement.click();
        Thread.sleep(2000);

        // TagName - https://www.amazon.com sitesine git Tum katagorıler listesine tıkla
        driver.get("https://www.amazon.com");
        driver.findElement(By.tagName("select")).click();
        Thread.sleep(2000);


        // ClassName - Google ana sayfasına gıt ve arama cubuguna tıkla.
        driver.get("https://www.google.com/");
        WebElement searchElement =driver.findElement(By.className("gLFyf"));
        searchElement.click();
        Thread.sleep(2000);

        // Link - facebook sıtesıne git ve "Şifreni mi Unuttun?" linkine tıkla.
        driver.get("https://tr-tr.facebook.com/");
        WebElement linkElement = driver.findElement(By.linkText("Şifreni mi Unuttun?"));
        linkElement.click();
        Thread.sleep(2000);

        // xpath - Google ana sayfasında "Kendimi Şanslı hissediyorum" butonuna tıkla
        driver.get("https://www.google.com/");
        WebElement buttonElement = driver.findElement(By.xpath("(//input[@class='RNmpXc'])[2]"));
        buttonElement.click();
        Thread.sleep(2000);

        // Css Selector - Amazon sıtesıne git ve tumu linkine tıkla.
        driver.get("https://www.amazon.com");
        WebElement tumuListesiElementi= driver.findElement(By.cssSelector("span[class='hm-icon-label']"));
        tumuListesiElementi.click();


    }
}
