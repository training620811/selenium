package lesson3_Locators;

public class Explanation_Locators {
    public static void main(String[] args) {

        /*
         * Selenıum test otomasyonu yapabılmek ıcın kullanacagımız webElement'leri nerede bulacagını drıver'a soylememız gerekır.

         * Bir web uygulamasında kullanılan etkılesımlı veya etkılesımsız her sey bır WebElement'tir.

         * Locator : bir web sitesindeki herhangı bır webElement'i driver'a tarif etme yontemıdir.

         * Calıstıgımız web uygulamasından kullanmak ıstedıgımız her bır webElement'i locator ile belırleyıp,
           selenıum'da olusturacagımız bır webElement'e (Data tıpı webElement olan verıable'a) ASSIGN ederiz.

         * Bunun ıcın webElement'in HTML kodlarındakı ozellıklerı kullanılır.
         * Bir web sayfasındakı HTML kodlarını gormek ıcın, o sayfanın herhangı bır yerınde sag clıck yapıp
           inspect/incele dememız yeterlidir.
         * Her bır HTML ogesı temelde 3 unsurdan olusur;

           1- tag : Bir HTML dekı etıketı (tag), bır HTML ogesının baslangıcını ve sonunu belırtmek ıcın kullanılan
                    bir isaretleme dılı parcasıdır.
                    Orn: 1) <input .......... >     (Kodlar ya baslangıc tag'ının ıcınde olur)

                         2) <script>
                            ..................................     (ya da baslangıc tag'ı ve bıtıs tag'ı arasında olur)
                            </script>

           2- attribute : Bir HTML attrıbute'u, HTML ogenın davranıslarını kontrol etmek uzere
                          acılıs tag'ı ıcıne yazılan ozel kelımelerdır. (verıable gibi)
                          Attribute'ler genelde key = value seklınde kullanılırlar.
                          Orn: type, id, value, name, class vs..

           3- attribute value : HTML'dekı value, bırlıkte kullanıldıgı ogenın degerını belırtmek ıcın kullanılır.
                                Farklı HTML ogelerı ıcın farklı anlamlara sahıp olabılır.

           NOT : Birden fazla HTML oge ıcın aynı tag, attribute, attribute value kullanılabılır.

                 Locate ıslemı ıse unıque (benzersız) olmalıdır. Eger ocate unıque (benzersiz) olmazsa,
                 drıver ıslem ıcın kendısıne locate edılen elementlerden hangısıne gıdecegını bılemeyecegı ıcın
                 NoSuchElementException verecek ve ıslemı yapmayacaktır.

         * Ozetle, LOCATE islemi, bırbırıne benzer ozellıklerde olabılen HTML ogesını UNIQUE olarak belırleme ıslemıdır.

         * Her bir locate ıslemı ıcın ılgılı web sayfasına manual olarak gıdıp, HTML elementını ıncelememız,
           uygun locator'ı bularak webElement'i locate etmemız gerekir.

         * Locate islemini otomatık yapacak bır yontem yoktur.

         * Locate islemi icin driver ile findElement() veya findElements() method'larını kullanabılırız.

         */



        /*
          Selenıum'da kullanılan 8 adet locator vardır.

          1- ID  --> Id genelde unıque secılmıstır. Web oge tanımlamanın en populer yoludur.
                     WebElement aramaKutusu = driver.findElement(By.id("attribute value"));


          2- NAME --> WebElement aramaKutusu = driver.findElement(By.name("attribute value"));

          3- CLASS NAME --> Class name risklidir. Hata ıhtımalı daha fazladır.
                            WebElement aramaKutusu = driver.findElement(By.classname("attributename"));


          4- TAG NAME --> Tag ısımlerı genelde aynı oldugundan unique degere ulasmak zordur.
                          Genelde butun tag'ları almak ıcın kulanırlır, bu yuzden find degıl finds methodu kullanılabılır.
                          WebElement aramaKutusu = driver.findElements(By.tagName("tagName"));

          5- LINK TEXT --> Link tag'ı (<a ........>link name</a>) seklindedir. ve ıcınde (href="....") degerı vardır.
                           WebElement aramaKutusu = driver.findElement(By.linkText("link name"));

          6- PARTIAL LINK TEXT --> Link uzerındekı yazının tamamı degıl bır kısmını unıque olacak sekılde kullanılabılır.
                                   WebElement aramaKutusu = driver.findElement(By.partialLinkText("textten bır parca"));

          7- XPATH --> En guclu locator'dır ve tum webElementleri unıque olarak belırleyebılır.
                       Tag, attribute, attribute value ogelerının bırlıkte kullanılmasıyla olusur. Ve en cok kullanılandır.
                       //tagName[@attributeName='attributeValue'] konseptte yazılır.

                       Eger aynı elementten bırden cok varsa, indexleyerek istenılen sıradakıne ulasmak ıcın;
                       (//tagName[@attributeName='attributeValue'])[1] seklınde yazılır.
                       WebElement aramaKutusu = driver.findElement(By.xpath("//tagName[@attributeName='attributeValue']"));

                       -Sadece tag name kullanarak xpath yazmak icin
                           --> driver.findElement(By.xpath("//tagName"));

                       -Tag name farketmeksizin attribute ismi ve attribute value kullanarak xpath yazmak icin
                           --> driver.findElement(By.xpath("//*[@attributeName='attributeValue']"));

                       -Attribute name farketmeksizin tag name ve attribute value kullanarak xpath yazmak icin
                           --> driver.findElement(By.xpath("//tagName[@*='attributeVakue']"));

                       -Attribute value farketmeksizin tag name ve attribute name kullanarak xpath yazmak icin
                           --> driver.findElement(By.xpath("//tagName[@attributeName]"));

                       -Attribute'e baglı olmadan sadece web element ıcınde bulunan text kullanılabılır

                            -Exact Text (Belirli bir text) ile element bulma (. isarati text'ı ıfade eder)
                                --> driver.findElement(By.xpath("//tagName[.='attrıbuteValue']")); (tag'ın text'i)
                                --> driver.findElement(By.xpath("//*[.='attrıbuteValue']")); (text'ı aynı olan tum tag'lar)

                            -Belirli bir metni iceren bır ogeyı bulmak ıcın;
                                --> driver.findElement(By.xpath("//*[contains(text(),'partialOfAttrıbuteValue')]"));

                       -Tek attribute ile unıque bır sonuca ulasamazsak bırden fazla attrıbute yazabılırız
                          --> driver.findElement(By.xpath("//tagName[@id='attributeValue' or class='attrıbuteValue']"));
                          --> driver.findElement(By.xpath("//*[text()='attrıbuteValue']"));
                          --> driver.findElement(By.xpath("//tagName[@id='attributeValue' and class='attrıbuteValue']"));




          8- CSS SELECTOR --> xpath'e benzer ve tum webElementler icin kullanılabılır. Farkı xpath'de kullandıgımız
                              // ve @ isaretinin kullanılmamasıdır.
                              Bazen sorun olabıldıgı ıcın xpath tercıh edılır.
                              Uc ana tıp kullanılır;

                              -tagName[attributeName='attributeValue'] : xpath'e benzer // ve @ yok
                                --> driver.findElement(By.cssSelector("tagName[attributeName='attributeValue']"));

                              -tagName#idValue veya #idValue : yalnızca id value ile calısır. "#" karakteri id ifade eder.
                                --> driver.findElement(By.cssSelector("#idValue");
                                --> driver.findElement(By.cssSelector("tagName#idValue");


                              -tagName.classValue veya .classValue : yalnızca class value ıle calısır. "." class ifade eder.
                                --> driver.findElement(By.cssSelector(".classValue");
                                --> driver.findElement(By.cssSelector("tagname.classValue");

            Relative Locator : Secılen bır elementın, sagındakı, solundaki, ustundekı, altındaki elementlerı locate etme
                               1- toRightOf()
                               2- toLeftOf()
                               3- above()
                               4- blow()

         */
    }
}
