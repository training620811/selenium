package lesson10_IFrame_HandleFrame;

public class explanation {
    /**
     *
     * IFrame : Bir web sayfasının ıcıne yerlestırılmıs baska bır web sayfası,
     *          diger bir ifade ıle bır HTML dokumanının ıcıne yerlestırılmıs baska bir HTML dokumanın olması
     *
     * Iframe'e drıver'ı gonderebılmek ıcın;
     *
     * 1- Iframe'ı locate etmek  -->   WebElement iframe =driver.findElement(.....);
     *
     * 2- driver'ı IFrame'e gondermek gerekir. 3 Sekılde gonderılır.
     *          - driver.switchTo().frame(iframe); --> iframe elementini parametre vermek
     *          - driver.switchTo().frame(index); --> iframe'in index'ini parametre vermek
     *          - driver.switchTo().frame(frameId); --> iframe'in id attribute'unun valuesunu parametre vermek.
     *
     * Not: IFrame gırdıkten sonra dıger WebElement ıslemlerı yapılabılır.
     * Not: Bir sayfadakı bırden fazla frame'ler arasında gecıs yapabılmek ıcın once en uste yanı defaulta gelmemız gerekir.
     *
     * Iframe'den drıvar'ı cıkartabılmek ıcın;
     * -Bir ust frame'e gecmek ıcın                        -->   driver.switchTo().parentFrame();
     * -Driver'ı ılk konumuna yanı en basa cıkartmak ıcın  -->   driver.switchTo().defaultContent();
     *
     */


}
