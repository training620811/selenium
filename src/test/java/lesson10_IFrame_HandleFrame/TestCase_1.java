package lesson10_IFrame_HandleFrame;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class TestCase_1 {

    /**
     * 1- https://the-internet.herokuapp.com/iframe URL gidin
     * 2- “An IFrame containing….” textinin erisilebilir oldugunu test edin ve  konsolda    yazdirin
     * 3- Text Box’a “Merhaba Dunya!” yazin.
     */

    WebDriver driver;

    @BeforeMethod
    public void setup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @Test
    public void test() {
        //1
        driver.get("https://the-internet.herokuapp.com/iframe");

        //2- “An IFrame containing….” textinin erisilebilir oldugunu test edin ve  konsolda    yazdirin
        WebElement titleText = driver.findElement(By.xpath("//div/h3[text()='An iFrame containing the TinyMCE WYSIWYG Editor']"));
        Assert.assertTrue(titleText.isDisplayed());
        System.out.println(titleText.getText());

        //3- Text Box’a “Merhaba Dunya!” yazin
        WebElement iframe = driver.findElement(By.tagName("iframe"));
        driver.switchTo().frame(iframe);
        WebElement textElement = driver.findElement(By.xpath("//p[text()='Your content goes here.']"));
        textElement.clear();
        textElement.sendKeys("Merhaba Dunya!");

        driver.switchTo().parentFrame(); // Bir ust frame'e cıkarttık. Bu ornekte ılk Html'ye

    }

    @AfterMethod
    public void tearDown() {
        driver.close();
    }
}
