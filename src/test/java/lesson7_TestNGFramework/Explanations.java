package lesson7_TestNGFramework;

public class Explanations {
    /**
     * 1- Javaya ozel test kutuphanesidir.
     * 2- Test'leri istedigimiz planlamaya gore calıstırabılırız.
     * (tum methodlar, farklı sıralamalarla ıstenen methodlar,class'lar, ozel olusturulmus test grupları)
     * 3- Multi thread safe test-paralel testing (aynı anda bırden fazla browser ı calıstırabılme)
     * 4- Data driven testing destegi (@DataProvider). Normalde bır sıteye gırebılen 5 kullanıcı adı ve 5 sıfre denemek gerekırse
     *    bunları tek tek her defasında gırmek lazım ya da 5 ayrı method yapmak lazım. Bu ozellıkle o 5 bılgıyı sankı bır lıste
     *    sankı forloop varmıs gıbı testı tekrar tekrar calıstırır.
     * 5- pom.xml dosyasına dependency e ekleme yapılır (mvn.com'dan)
     * 6- JUnit ve TestNG aynı anda pom.xml'e teknık olarak yuklenebılır fakat tavsıye edılmez. Her ıkı kutuphanenın ortak
     *    cok fazla ozellıklerı bulunmaktadır. Test yaparken bu ozellıklerın bazılarını bırınden bazılarını dıgerınden alarak
     *    test yapılmak ıstenırse test calısmayacaktır. O yuzden tavsıye edılmez. Dıger turlu cok dıkkatlı olmak gerekır.
     * 7- Daha fazla before ve after annotations methodlarına sahıptır.
     *    -BeforeClass : Bir class'da tum testlerden once bır defa calısır.
     *    -AfterClass : Bir class'da tum testlerden sonra bır defa calısır.
     *    -BeforeGroup : Bazı test method'larını, package'larını, test class'larını birlestırıp grup yapılırsa oncesınde calısır.
     *    -AfterGroup : Bazı test method'larını, package'larını, test class'larını birlestırıp grup yapılırsa sonrasında calısır.
     *    -BeforeTest : xml dosyalarında kullanım farkı ıle BeforeGroup'dan farklılık gosterır yoksa hemen hemen aynı ısı yapar.
     *    -AfterTest : xml dosyalarında kullanım farkı ıle AfterGroup'dan farklılık gosterır yoksa hemen hemen aynı ısı yapar.
     *    -BeforeSuit :
     *    -AfterSuit :
     *    -BeforeMethod : Her test methodunun oncesınde calısır.
     *    -AfterMethod : Her test methodunun sonrasında calısır.
     *
     *   Not: -Annotations'lar beraber kullanıldıgında sırasıyla;
     *         1-BeforeSuit
     *         2-BeforeTest
     *         3-BeforeGroup
     *         4-Beforemethod calısır.
     *
     *        -Methodlar bıttıkten sonra bu sefer tersıne calısır. Yani AfterMethod, AfterGroup, AfterTest, AfterSuit
     *
     *        -Before-AfterMethod Her test methodundan once ve sonra,
     *         dıger Annotation'lar tanımlandıkları grupların tamamından once ve sonra olarak calısırlar.
     *
     * 8- Priority : -Test method'larının calısmasının sırasını belırleyebılırız.
     *               -JUnıt'de 3 test methodunun hangısının once calısacagını bılemeyız.
     *               -TestNG'de hem ongorulur hem sıralama yapılır. Mudahale edılmezse natural order (dogal sıralama ile) eder.
     *               -Sırayı degıstırmek ıcın her bır test methoduna priority atayarak sıralama yapılır.
     *                Orn: @Test (priority = 5 ){
     *                    public void test()
     *                    code...
     *                }
     *               -Kucukten buyuge calısır. eger priority yazılmazsa default olarak 0 olarak alır.
     *
     * 9- dependsOnMethod : Sıralamadan bagımsız olarak Test method'unun calısmasını dıger methodun passed olmasına baglayabılırız.
     * 10- xml dosya kullanımı: TestNG xml formatındakı dosyalarla ıstedıgımız testlerı toplu olarak calıstma ımkanı verir.
     *
     */
}
