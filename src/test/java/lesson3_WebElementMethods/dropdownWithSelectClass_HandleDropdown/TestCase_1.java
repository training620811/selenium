package lesson3_WebElementMethods.dropdownWithSelectClass_HandleDropdown;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import java.util.List;

public class TestCase_1 {
    /**
     * Dropdown elementinın ıcındekı elementlere erismek icin;
     *  1-Dropdown elementi locate edilir.
     *  2-Select class'ından parametreli obje olusturmamız gerekmektedir. Parametresi dropdown elementidir.
     *      Select select =new Select(dropdown element);
     *
     *  3-Olusturulan select objesi ile 3 sekilde dropdown elementleri bulunur;
     *     A- Index: dropdown'daki elementin sırası ile.
     *               select.selectByIndex(index);
     *     B- Value: dropdown'daki elementin DOM'daki Value Attribute'unun value'su ile.
     *               selectByValue("Value");
     *     C- VisibleText: dropdown'daki elementin DOM'daki text'i ile.
     *                     selectByVisibleText("VisibleText");
     *     D- Eger dropdown elementinin text'ini yazdırmak ıstersek;
     *                     select.getFirstSelectedOption().getText()
     *
     *     E- Eger tüm option(dropdown elementlerini) getirmek istersek liste'de tutarız. Yazdırmak ıcın loop kullanılır.
     *                     List<WebElement> optionList =select.getOptions();
     */

    @Test
    public void test() throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.get("https://www.amazon.com/");



        // 1. islem Dropdown locate islemi yapılır
        WebElement dropdownElement = driver.findElement(By.id("searchDropdownBox"));

        // 2.islem Select classından bir parametreli(dropdownElementi olacak) obje olustruyorum
        Select select = new Select(dropdownElement);

        // 1.yontem
        select.selectByIndex(3);
        System.out.println("3.indexteki Eleman : " + select.getFirstSelectedOption().getText()); // Baby
        Thread.sleep(2000);

        //2.yontem
        select.selectByValue("search-alias=stripbooks-intl-ship");
        System.out.println("Value : " + select.getFirstSelectedOption().getText()); // Books
        Thread.sleep(2000);

        //3.yontem
        select.selectByVisibleText("Digital Music");
        System.out.println("VisibleText : " + select.getFirstSelectedOption().getText());//Digital Music
        Thread.sleep(2000);

        // Tum option getirmek istiyorum
        List<WebElement> optionList = select.getOptions();
        int sayac = 0;
        System.out.println("\nTUM LISTE\n");

        for(WebElement w : optionList){
            System.out.println(sayac+" : " + w.getText());
            sayac++;
        }

        driver.close();

    }
}
