package lesson3_WebElementMethods.checkBox;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;
import java.util.List;

public class TestCase_2 {
    /**
     * 1- https://demoqa.com/ sayfasına gidin.
     * 2- Elements butonuna basın.
     * 3- Check Box linkine basın.
     * 4- Home chekbox'ının tıklanılıp tıklanılmadıgını kontrol edin.
     * 5- Eger tıklanıyorsa home checkbox'ına tıklayın.
     * 6- Sonra tıklı oldugunu test edin.
     * 7- output yazdırın. Ve ilk kelimenin "home" oldugunu test edin.
     * 8- toggleButton'a tıklayın ve Home checkbox'ın altındakı Desktop, Documents, Download checkbox'larının gorunurlugunu kontrol edin.
     * 9- Home checkbox'ına tıklayın ve sonra sadece Desktop checkbox'ını secin
     * 10- Home checkbox'ın secılmemıs oldugunu dogrulayın.
     * 11- Sayfayı refresh edin
     * 12- Sayfayı kapatın.
     */

    public static void main(String[] args) throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

        //1- https://demoqa.com/ sayfasına gidin.
        String url = "https://demoqa.com/";
        driver.navigate().to(url);

        //2- Elements butonuna basın.
        driver.findElement(By.xpath("(//div[@class='avatar mx-auto white'])[1]")).click();
        Thread.sleep(2000);

        //3- Check Box linkine basın.
        driver.findElement(By.xpath("(//li[@id='item-1']/span)[1]")).click();
        Thread.sleep(2000);

        //4- Home chekbox'ının tıklanabılırlıgını kontrol edin.
        WebElement homeCheckBox = driver.findElement(By.className("rct-title"));
        if (homeCheckBox.isEnabled()) {
            System.out.println("Home checkbox is active");
        } else {
            System.out.println("Home checkbox is passive");
        }
        Thread.sleep(2000);


        //5- Eger tıklanıyorsa tıklı olup olmadıgını kontrol edin, tıklanmıs degılse home checkbox'ına tıklayın.
        boolean isSelectedCheckbox = homeCheckBox.isSelected();
        if (isSelectedCheckbox) {
            System.out.println("Home checkbox is already selected!");
        } else {
            homeCheckBox.click();
            System.out.println("Home checkbox is selected now!");
        }
        System.out.println("-----------------------------------------");
        Thread.sleep(2000);

        //6- Sonra tıklı oldugunu test edin.

        WebElement actualHomeCheckbox = driver.findElement(By.cssSelector("label[for='tree-node-home'] span.rct-checkbox svg"));
        String expectedCheckBoxClassValue = "rct-icon rct-icon-check";
        String actualCheckBoxClassValue = actualHomeCheckbox.getAttribute("class");

        if (actualCheckBoxClassValue.equals(expectedCheckBoxClassValue)) {
            System.out.println("Home Checkbox : Selected!");
        } else {
            System.out.println("Home Checkbox : Unselected!");
        }

        //7- output yazdırın. Ve ilk kelimenin "home" oldugunu test edin.
        List<WebElement> resultHome = driver.findElements(By.className("text-success"));
        System.out.println(driver.findElement(By.xpath("//span[text()='You have selected :']")).getText());
        for (WebElement result : resultHome) {
            System.out.println(result.getText());
        }
        String actualFirstResult = resultHome.get(0).getText();
        String expectedFirstResult = "home";

        if (actualFirstResult.equals(expectedFirstResult)){
            System.out.println("First result is PASSED");
        }else {
            System.out.println("First result is FAILED");
        }
        Thread.sleep(2000);

        //8- toggleButton'a tıklayın
        // ve Home checkbox'ın altındakı Desktop, Documents, Download checkbox'larının gorunurlugunu kontrol edin.

        WebElement toggleButton = driver.findElement(By.xpath("//button[@title='Toggle']"));
        toggleButton.click();
        Thread.sleep(1000);

        WebElement checkBoxDesktop = driver.findElement(By.xpath("//label[@for='tree-node-desktop']"));
        WebElement checkBoxDocuments = driver.findElement(By.xpath("//label[@for='tree-node-documents']"));
        WebElement checkBoxdownloads = driver.findElement(By.xpath("//label[@for='tree-node-downloads']"));

        if(checkBoxDesktop.isDisplayed() && checkBoxDocuments.isDisplayed() && checkBoxdownloads.isDisplayed()){
            System.out.println("Checkboxes visible!");
        }else {
            System.out.println("Checkboxes invisible!");
        }
        Thread.sleep(2000);

        //9- Home checkbox'ına tıklayın ve sonra sadece Desktop checkbox'ını secin
        homeCheckBox.click();
        Thread.sleep(2000);

        checkBoxDesktop.click();
        Thread.sleep(2000);

        //10- Home checkbox'ın secılmemıs oldugunu dogrulayın.
        if(homeCheckBox.isSelected()){
            System.out.println("already selected");
        }else {
            System.out.println("not selected");
        }
        Thread.sleep(2000);

        //11- Sayfayı refresh edin
        driver.navigate().refresh();
        Thread.sleep(2000);

        //12- Sayfayı kapatın.
        driver.close();

    }
}
