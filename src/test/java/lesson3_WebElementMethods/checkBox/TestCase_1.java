package lesson3_WebElementMethods.checkBox;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class TestCase_1 {
    /**
     * 1- https://the-internet.herokuapp.com/ sayfasına gidin. Ve Checkboxes lınkıne tıklayın.
     * 2- Checkbox1 ve checkbox2 elementlerini locate edin.
     * 3- Checkbox1 seçili değilse onay kutusunu tıklayın.
     * 4- Checkbox2 seçili değilse onay kutusunu tıklayın.
     * 5- Checkbox1 ve CheckBox2'nın secılı oldugunu test edin. Eger checkbox'lar secılıyse sayfayı refresh yapın.
     * 6- Sayfayı kapatın.
     */
    public static void main(String[] args) throws InterruptedException {

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

        //1- https://the-internet.herokuapp.com/ sayfasına gidin. Ve Checkboxes lınkıne tıklayın.
        String url = "https://the-internet.herokuapp.com/";
        driver.navigate().to(url);
        driver.findElement(By.linkText("Checkboxes")).click();
        Thread.sleep(2000);

        //2- Checkbox1 ve checkbox2 elementlerini locate edin.
        WebElement checkBox1 = driver.findElement(By.xpath("(//input[@type='checkbox'])[1]"));
        WebElement checkBox2 = driver.findElement(By.xpath("(//input[@type='checkbox'])[2]"));

        //3- Checkbox1 seçili değilse onay kutusunu tıklayın.
        if(!checkBox1.isSelected()){
            checkBox1.click();
            Thread.sleep(2000);
        }

        //4- Checkbox2 seçili değilse onay kutusunu tıklayın.
        if(!checkBox2.isSelected()){
            checkBox2.click();
        }
        Thread.sleep(2000);


        //5- Checkbox1 ve CheckBox2'nın secılı oldugunu test edin. Eger checkbox'lar secılıyse sayfayı refresh yapın.
        if(checkBox1.isSelected() && checkBox2.isSelected()){
            System.out.println("CheckBoxes is checked!");
            driver.navigate().refresh();
        }else {
            System.out.println("CheckBoxes is unchecked!");
        }
        Thread.sleep(2000);

        //6- Sayfayı kapatın.
        driver.close();

    }
}
