package lesson3_WebElementMethods.explanation;

public class Methods {

    /**
     * sendKeys("string") : Textbox'a yazı yazmak ıcın kullanılır.
     * sendKeys("string", Key.ENTER) : Hem Text doldurur hem de enter basar.
     * submit() : Enter'a basar.
     * click() : Tıklama ıslemı yapar.
     * getText() : Ilgili Element'in text'ini String olarak ceker/döndürür.
     * getAttribute("Attribute Name") : ilgili elementteki istenilen attribute'un value'sunu string olarak verir.
     * isEnabled() : CheckBox eger tıklanabılıyorsa true, tıklanamıyorsa false dondurur.
     * isSelected() : Eger checkbox (default) secılıyse true, secılı degılse false dondurur.
     * isDisplayed() : webelementin gozukup gozukmedıgını true veya false olarak dondurur.
     */
}
