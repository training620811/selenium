package lesson3_WebElementMethods.brokenImages;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Duration;

public class ExampleBrokenImage {
    public static void main(String[] args) throws IOException {

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));

        driver.get("https://demoqa.com/broken");

        WebElement link = driver.findElement(By.xpath("(//div//img)[3]"));

        String linkUrl = link.getAttribute("src");

        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(linkUrl).openConnection();
            connection.setRequestMethod("HEAD");
            connection.connect();

            int responseCode = connection.getResponseCode();
            System.out.println("response code : " + responseCode);

            if (responseCode > 400) {
                System.out.println("INVALID LINK");
            } else {
                System.out.println("VALID LINK");
            }
        }catch (IOException e){
            System.out.println("Error checking response code for URL " + linkUrl);
        }



    }
}
