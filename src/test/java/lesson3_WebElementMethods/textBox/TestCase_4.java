package lesson3_WebElementMethods.textBox;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;

public class TestCase_4 {

    /**
     * 1- Amazon web sayfasına gıdın https://www.amazon.com.tr
     * 2- cerezler varsa kabul edın.
     * 3- Search kutusuna "laptop" yazıp aratın
     * 4- Goruntulenen ılgılı sonucların sayısını yazdırın
     * 5- Karsınıza cıkan ılk sonucun resmıne tıklayın
     * 6- 2 sn bekleyın ve browser ı kapatın.
     */
    public static void main(String[] args) throws InterruptedException {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");

        WebDriver driver = new ChromeDriver(chromeOptions);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));

        // 1-
        driver.get("https://www.amazon.com.tr");

        // 2-
        driver.findElement(By.id("sp-cc-accept")).click();

        // 3-
        WebElement searchBox = driver.findElement(By.id("twotabsearchtextbox"));
        searchBox.sendKeys("laptop");
        searchBox.submit();

        // 4-

        String aramaSonucuText = driver.findElement(By.xpath("(//div[@class='a-section a-spacing-small a-spacing-top-small']/span)[1]")).getText() + " " +
                                driver.findElement(By.xpath("(//div[@class='a-section a-spacing-small a-spacing-top-small']/span)[3]")).getText();
        System.out.println("Arama sonucu : " + aramaSonucuText);

        // 5-
        driver.findElement(By.xpath("(//div[@class='a-section aok-relative s-image-square-aspect'])[1]")).click();
        Thread.sleep(2000);

        // 6-
        driver.quit();
    }
}
