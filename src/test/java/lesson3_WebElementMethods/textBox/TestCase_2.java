package lesson3_WebElementMethods.textBox;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

        /**
         * 1- https://www.facebook.com adresıne gıdın.
         * 2- cookies cıkarsa kabul et butonuna basın
         * 3- e-posta kutusuna rastgele bır mail gırın.
         * 4- sifre kısmına rastgele bır sıfre gırın.
         * 5- gırıs yap butonuna basın
         * 6- uyarı olarak "Invalid username or password" mesajının cıktıgını test edin.
         * 7- sayfayi kapatın.
         */
public class TestCase_2 {
    public static void main(String[] args) {

        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));

        // 1- https://www.facebook.com adresıne gıdın.
        driver.get("https://www.facebook.com");

        // 2- cookies cıkarsa kabul et butonuna basın. (Suan yok.)

        // * Siteyi ing dile cevır
        driver.findElement(By.linkText("English (UK)")).click();

        // 3- e-posta kutusuna rastgele bır mail gırın.
        WebElement epostaKutusu= driver.findElement(By.xpath("//input[@id='email']"));
        epostaKutusu.sendKeys("rastgele");

        // 4- sifre kısmına rastgele bır sıfre gırın.
        WebElement sifre = driver.findElement(By.xpath("//input[@id='pass']"));
        sifre.sendKeys("123456789");

        // 5- gırıs yap butonuna basın
        driver.findElement(By.xpath("//button[@value='1']")).click();

        //6- uyarı olarak "Invalid username or password mesajının cıktıgını test edin.
        WebElement uyari =driver.findElement(By.xpath("//*[@id='error_box']/div[2]"));
        String actual = uyari.getText();
        String expected ="Invalid username or password";
        if(actual.equals(expected)){
            System.out.println("Girilemedi testi : PASSED");
        }else{
            System.out.println("Girilemedi testi : FAILED");
        }

        // 7- Sayfayı kapat.
        driver.close();
    }
}
