package lesson3_WebElementMethods.textBox;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;

public class TestCase_3 {

    /**
     * 1- https://www.amazon.com git.
     * 2- Varsa cerezlerı kabul et.
     * 3- Arama butonuna iphone 11 yaz ve arama ıslemını yap.
     * 4- Arama sonucunun iphone 11 icerip icermedigini kontol edin.
     * 5- İlk urunun fıyatını yazdırın
     * 6- 2 sn bekletın ve browser ı kapatın.
     */
    public static void main(String[] args) throws InterruptedException {

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");

        WebDriver driver = new ChromeDriver(chromeOptions);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));


        // 1- https://www.amazon.com git.
        driver.get("https://www.amazon.com.tr/");

        // 2- cerezlerı kabul et.
        driver.findElement(By.id("sp-cc-accept")).click();

        // 3- Arama butonuna iphone 11 yaz ve arama ıslemını yap.
        WebElement aramaKutusu = driver.findElement(By.id("twotabsearchtextbox"));
        aramaKutusu.sendKeys("iphone 11", Keys.ENTER);          // hem text doldurur hem de enter basar.

        // 4- Arama sonucunun iphone 11 icerip icermedigini kontol edin.
        String aramaSonucu = driver.findElement(By.xpath("//div[@class='a-section a-spacing-small a-spacing-top-small']")).getText();
        System.out.println("Arama sonucu : " + aramaSonucu);

        if (aramaSonucu.contains("iphone 11")) {
            System.out.println("Arama sonucu iphone 11 iceriyor, Test : PASSED");
        } else {
            System.out.println("Arama sonucu iphone 11 icermiyor, Test : FAILED");
        }


        // 5- İlk urunun fıyatını yazdırın
        String lira = driver.findElement(By.xpath("(//span[@class='a-price-whole'])[2]")).getText();
        String kurus = driver.findElement(By.xpath("(//span[@class='a-price-fraction'])[2]")).getText();
        String TL = driver.findElement(By.xpath("(//span[@class='a-price-symbol'])[2]")).getText();
        System.out.println("İlk urun fiyatı : " + lira + "," + kurus + " " + TL);


        // 6- 2 sn bekletın ve browser ı kapatın.
        Thread.sleep(2000);
        driver.quit();
    }
}
