package lesson3_WebElementMethods.textBox;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;
import java.util.List;

/**
         * 1- https://www.bestbuy.com adresıne gidin.
         * 2- cookies cıkarsa kabul et butonuna basın
         * 3- sayfada kac adet button tagı bulundugunu yazdırın.
         * 4- sayfadakı herbır button tagı uzerındekı yazıları yazdırın.
         * 5- sayfayı kapatın.
         */


public class TestCase_6 {
    public static void main(String[] args) {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");

        WebDriver driver = new ChromeDriver(chromeOptions);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));

        // 1- siteye gidin
        driver.get("https://www.bestbuy.com");

        // 2- cookıes yok

        // 3- sayfada kac adet button bulundugunu yazdırın.
        List<WebElement> buttonListesi= driver.findElements(By.tagName("button"));  //liste olusturduk.
        System.out.println("Sayfadaki button sayısı : " + buttonListesi.size());

        // 4- sayfadakı herbır buttonların uzerındekı yazıları yazdırın.
        for (WebElement button : buttonListesi){
            System.out.println(button.getText());
        }

        // 5- sayfayı kapatın.
        driver.close();


    }
}
