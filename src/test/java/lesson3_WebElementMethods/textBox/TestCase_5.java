package lesson3_WebElementMethods.textBox;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class TestCase_5 {
    /**
     * 1- https://demoqa.com/ adresine gidin.
     * 2- Elements path'ına tıklayın.
     * 3- "Please select an item from left to start practice." text'inin olup olmadıgını kontrol edin.
     * 4- Text Box linkine tıklayın.
     * 5- Rastgele olacak sekılde, Full Name, Email, Current Address, Permanent Address textbox'larını doldurun.
     * 6- Submit butonuna basın.
     * 7- Sonucların girilen ifadelerle aynı olup olmadıgını kontrol edin
     * 8- Sayfayı kapatın.
     */


    public static void main(String[] args) throws InterruptedException {

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        JavascriptExecutor jsx = (JavascriptExecutor) driver;

        // 1- https://demoqa.com/ adresine gidin.
        driver.navigate().to("https://demoqa.com/");
        jsx.executeScript("window.scrollBy(0,300)");    // sayfayı kaydırmak ıcın.
        Thread.sleep(2000);


        // 2- Elements path'ına tıklayın.
        WebElement elementsPath = driver.findElement(By.xpath("//h5[text()='Elements']"));
        elementsPath.click();
        Thread.sleep(2000);

        // 3- "Please select an item from left to start practice." text'inin olup olmadıgını test edin.
        String expectedText = "Please select an item from left to start practice.";
        String actualText = driver.findElement(By.xpath("//div[@class='col-12 mt-4 col-md-6']")).getText();
        if (actualText.equals(expectedText)) {
            System.out.println("Text testi PASSED");
        } else {
            System.out.println("Text testi FILED");
        }

        // 4- Text Box linkine tıklayın.
        driver.findElement(By.xpath("//span[text()='Text Box']")).click();
        jsx.executeScript("window.scrollBy(0,150)");    // sayfayı kaydırmak ıcın.
        Thread.sleep(2000);

        // 5- Rastgele olacak sekılde, Full Name, Email, Current Address, Permanent Address textbox'larını doldurun.
        String fullName = "izzet";
        String email = "erol@gmail.com";
        String currentAddress = "Istanbul";
        String permanentAddress = "Turkiye";
        driver.findElement(By.xpath("//input[@id='userName']")).sendKeys(fullName);
        driver.findElement(By.xpath("//input[@id='userEmail']")).sendKeys(email);
        driver.findElement(By.xpath("//textarea[@id='currentAddress']")).sendKeys(currentAddress);
        driver.findElement(By.xpath("//textarea[@id='permanentAddress']")).sendKeys(permanentAddress);

        // 6- Submit butonuna basın
        driver.findElement(By.id("submit")).click();

        // 7- Sonucların girilen ifadelerle aynı olup olmadıgını kontrol edin
        jsx.executeScript("window.scrollBy(0,300)");    // sayfayı kaydırmak ıcın.

        String expectedName = "Name:" + fullName;
        String expectedEmail = "Email:" + email;
        String expectedCurrentAddress = "Current Address :" + currentAddress;
        String expectedPermanentAddress = "Permananet Address :" + permanentAddress;

        String actualName = driver.findElement(By.xpath("//p[@id='name']")).getText();
        String actualEmail = driver.findElement(By.xpath("//p[@id='email']")).getText();
        String actualCurrentAddress = driver.findElement(By.xpath("//p[@id='currentAddress']")).getText();
        String actualPermanentAddress = driver.findElement(By.xpath("//p[@id='permanentAddress']")).getText();

        if (actualName.equals(expectedName) &&
                actualEmail.equals(expectedEmail) &&
                actualCurrentAddress.equals(expectedCurrentAddress) &&
                actualPermanentAddress.equals(expectedPermanentAddress)) {
            System.out.println("PASSED");

        } else {
            System.out.println("FAILED");
        }
        Thread.sleep(2000);

        // 8- sayfaya refresh atın
        driver.navigate().refresh();

        // 9- cıktı kısmının bos olup olmadıgını test edın.
        String output= driver.findElement(By.id("output")).getText();
        System.out.println(output.isEmpty());

        // 10- sayfayı kapatın.
        driver.quit();
    }
}
