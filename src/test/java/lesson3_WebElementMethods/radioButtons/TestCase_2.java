package lesson3_WebElementMethods.radioButtons;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class TestCase_2 {
    /**
     * 1- https://demoqa.com/radio-button sayfasına git
     * 2- Yes radiobutton'unun tıklanabılır (etkin) olup olmadıgını kontrol edin. Tıklanıyorsa tıklama yapın.
     * 3- Yes radioButton'unun tıklı oldugunu test edin
     * 4- Sayfayı kapatın
     */

    public static void main(String[] args) throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

        //1- https://demoqa.com/radio-button sayfasına git
        String url = "https://demoqa.com/radio-button";
        driver.navigate().to(url);
        Thread.sleep(2000);

        WebElement yesRadioButtonLabel = driver.findElement(By.xpath("//label[@for='yesRadio']"));
        Thread.sleep(2000);


        //2- Yes radiobutton'unun tıklanabılır (etkin) olup olmadıgını kontrol edin. Tıklanıyorsa tıklama yapın.
        Boolean isEnableYesRadioButton = yesRadioButtonLabel.isEnabled();
        if (isEnableYesRadioButton) {
            System.out.println("yesRadioButton is enable");
            yesRadioButtonLabel.click();
            System.out.println("yesRadioButton is clicked!");
        } else {
            System.out.println("yesRadioButton is enable");
        }

        //3- Yes radioButton'unun tıklı oldugunu test edin

        WebElement yesRadioButton = driver.findElement(By.xpath("//input[@id='yesRadio']"));
        Boolean isSelectedYesRadioButton = yesRadioButton.isSelected();
        System.out.println("Is yesRadioButton selected : " + isSelectedYesRadioButton);


        //4- Sayfayı kapatın.
        Thread.sleep(2000);
        driver.close();

    }
}
