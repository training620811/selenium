package lesson3_WebElementMethods.radioButtons;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class TestCase_1 {
    /**
     * https://www.facebook.com adresine gidin
     * “Yeni Hesap Oluşturun” button’una basin
     * “radio buttons” elementlerini locate edin
     * Secili degilse cinsiyet butonundan size uygun olani secin
     * İlgiki radiobutton'un secili olup olmadıgını test edin.
     * Sayfayı kapatın.
     */

    public static void main(String[] args) throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));


        //       1- https://www.facebook.com adresine gidin
        driver.navigate().to("https://www.facebook.com");
        Thread.sleep(2000);


        //       2- “Yeni Hesap Oluşturun” button’una basin

        WebElement yeniHesapOlusturButton = driver.findElement(By.xpath("//a[text()='Yeni hesap oluştur']"));
        yeniHesapOlusturButton.click();
        Thread.sleep(2000);


        //       3- “radio buttons” elementlerini locate edin
        WebElement erkekRadioButton = driver.findElement(By.xpath("//input[@value='2']"));
        Thread.sleep(1000);


        //       4- Secili degilse cinsiyet butonundan size uygun olani secin
        if (!erkekRadioButton.isSelected()) {
            erkekRadioButton.click();
        } else {
            System.out.println("RadioButton already selected!");
        }
        Thread.sleep(2000);

        //      5- İlgiki radiobutton'un secili olup olmadıgını test edin.
        System.out.println("Is radioButton selected : " + erkekRadioButton.isSelected());


        //      6- sayfayı kapatın.
        driver.close();
    }
}
