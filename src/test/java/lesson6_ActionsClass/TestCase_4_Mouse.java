package lesson6_ActionsClass;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class TestCase_4_Mouse {

    /**
     * 1- https://selenium08.blogspot.com/2020/01/click-and-hold.html
     * 2- Locate the element C ve Element D.
     * 3- C elementini D elementinin uzerinde bekletin
     */

    WebDriver driver;

    @BeforeMethod
    public void setup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @Test
    public void test() throws InterruptedException {
        //1
        driver.get("https://selenium08.blogspot.com/2020/01/click-and-hold.html");

        //2
        WebElement titleC= driver.findElement(By.cssSelector("li[name='C']"));
        WebElement titleD= driver.findElement(By.cssSelector("li[name='D']"));
        Actions actions = new Actions(driver);
        actions.clickAndHold(titleC)
                .moveToElement(titleD)
                .perform();
        Thread.sleep(2000);

    }

    @AfterMethod
    public void tearDown() {
       driver.close();
    }

}
