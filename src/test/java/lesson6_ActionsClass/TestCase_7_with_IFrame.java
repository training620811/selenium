package lesson6_ActionsClass;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class TestCase_7_with_IFrame {

    /**
     * 1- https://html.com/tags/iframe/ sayfasina gidelim
     * 2- video’yu gorecek kadar asagi inin
     * 3- videoyu izlemek icin Play tusuna basin
     * 4- videoyu calistirdiginizi test edin
     * 5- videoyu durdurun
     * 6- Browser'ı kapatın
     */


    WebDriver driver;

    @BeforeMethod
    public void setup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @Test
    public void test() throws InterruptedException {
        //1- https://html.com/tags/iframe/ sayfasina gidelim
        driver.get("https://html.com/tags/iframe/");

        //2- video’yu gorecek kadar asagi inin
        Actions action = new Actions(driver);
        action.sendKeys(Keys.PAGE_DOWN)
                .sendKeys(Keys.PAGE_DOWN)
                .sendKeys(Keys.ARROW_UP)
                .perform();

        //3- videoyu izlemek icin Play tusuna basin
        WebElement iframeVideo = driver.findElement(By.xpath("//iframe[@width='560']"));
        driver.switchTo().frame(iframeVideo);

        WebElement playButton = driver.findElement(By.xpath("//button[@aria-label='Oynat']"));
        playButton.click();
        Thread.sleep(2000);

        //4- videoyu calistirdiginizi test edin
        WebElement stopButton = driver.findElement(By.xpath("//button[@title='Duraklat (k)']"));
        Assert.assertTrue(stopButton.isEnabled());

        //5- videoyu durdurun.
        stopButton.click();
        driver.switchTo().parentFrame();
        Thread.sleep(2000);

    }

    @AfterMethod
    public void tearDown() {
        //6- sayfayı kapatın
        driver.close();
    }
}
