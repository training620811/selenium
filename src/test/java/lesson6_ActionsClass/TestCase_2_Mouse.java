package lesson6_ActionsClass;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import java.time.Duration;

public class TestCase_2_Mouse {

    /**
     * 1- Amazon ana sayfasına gidin. https://www.amazon.com/
     * 2- Sag ustte Hello,sign in elementinin uzerinde mouse'u bekletin
     * 3- Acilan menu de Create a list linkine tiklayin
     * 4- Create a list sayfasini acildigini test edin
     */

    WebDriver driver;

    @BeforeMethod
    public void setup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @Test
    public void test() throws InterruptedException {
        //1-
        driver.get("https://www.amazon.com/");

        //2- Sag ustte Hello,sign in elementinin uzerinde mouse'u bekletin
        //3- Acilan menu de Create a list linkine tiklayin
        Actions action = new Actions(driver);

        WebElement helloTextElement = driver.findElement(By.xpath("//span[text()='Hello, sign in']"));
        WebElement creatAListText = driver.findElement(By.xpath("//span[text()='Create a List']"));

        action.moveToElement(helloTextElement) //2
                .click(creatAListText)         //3
                .perform();
        Thread.sleep(2000);

        //4- Create a list sayfasini acildigini test edin
        String expectedUrl = "https://www.amazon.com/hz/wishlist/intro";
        String actualUrl = driver.getCurrentUrl();
        Assert.assertEquals(actualUrl, expectedUrl, "URL Test");

    }

    @AfterMethod
    public void tearDown() {
        driver.close();
    }

}
