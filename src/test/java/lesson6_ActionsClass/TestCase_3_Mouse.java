package lesson6_ActionsClass;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class TestCase_3_Mouse {
    /**
     * 1- https://the-internet.herokuapp.com/context_menu sitesine gidelim
     * 2- Cizili alan uzerinde sag click yapalim
     * 3- Alert’te cikan yazinin “You selected a context menu” oldugunu test edelim.
     * 4- Tamam diyerek alert’i kapatalim.
     */

    WebDriver driver;

    @BeforeMethod
    public void setup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @Test
    public void test() {

        //1- https://the-internet.herokuapp.com/context_menu sitesine gidelim
        driver.get("https://the-internet.herokuapp.com/context_menu");

        //2- Cizili alan uzerinde sag click yapalim
        Actions actions = new Actions(driver);
        WebElement ciziliAlan = driver.findElement(By.id("hot-spot"));
        actions.contextClick(ciziliAlan).perform();

        //3- Alert’te cikan yazinin “You selected a context menu” oldugunu test edelim.
        String expectedResult = "You selected a context menu";
        String actualResult = driver.switchTo().alert().getText();
        Assert.assertEquals(actualResult,expectedResult,"Result Test");

        //4- Tamam diyerek alert’i kapatalim.
        driver.switchTo().alert().accept();

    }

    @AfterMethod
    public void tearDown() {
        driver.close();
    }
}
