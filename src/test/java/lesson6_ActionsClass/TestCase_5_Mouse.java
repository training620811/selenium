package lesson6_ActionsClass;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class TestCase_5_Mouse {

    /**
     * 1- https://demoqa.com/droppable adresine gidelim
     * 2- “Drag me” butonunu tutup “Drop here” kutusunun ustune birakalim
     * 3- “Drop here” yazisi yerine “Dropped!” oldugunu test edin
     */

    WebDriver driver;

    @BeforeMethod
    public void setup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @Test
    public void test(){

        //1-https://demoqa.com/droppable adresine gidelim
        driver.get("https://demoqa.com/droppable");

        //2-“Drag me” butonunu tutup “Drop here” kutusunun ustune birakalim
        WebElement dragMeElement = driver.findElement(By.id("draggable"));
        WebElement dropHereElement = driver.findElement(By.xpath("(//p[text()='Drop here'])[1]"));
        Actions actions = new Actions(driver);
        actions.dragAndDrop(dragMeElement,dropHereElement).perform();

        //3- “Drop here” yazisi yerine “Dropped!” oldugunu test edin
        String expectedText ="Dropped!";
        String actualText= dropHereElement.getText();
        Assert.assertEquals(actualText,expectedText,"Text Test");

    }

    @AfterMethod
    public void tearDown(){
        driver.close();
    }
}
