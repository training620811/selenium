package lesson6_ActionsClass;

import com.beust.ah.A;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.security.Key;
import java.time.Duration;

public class TestCase_6_Keyboard {
    /**
     * 1- https://www.amazon.com sayfasina gidelim
     * 2- Arama kutusuna actions method’larine kullanarak Samsung A71 yazdirin ve Enter’a basarak arama yaptirin
     * 3- aramanin gerceklestigini test edin
     * 4- Browser kapatılır
     */

    WebDriver driver;

    @BeforeMethod
    public void setup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @Test
    public void test() {

        //1- https://www.amazon.com sayfasina gidelim
        driver.get("https://www.amazon.com");

        //2- Arama kutusuna actions method’larine kullanarak Samsung A71 yazdirin ve Enter’a basarak arama yaptirin
        WebElement searchBox = driver.findElement(By.id("twotabsearchtextbox"));
        Actions actions = new Actions(driver);
        actions.click(searchBox)
                .sendKeys("Samsung")
                .sendKeys(Keys.SPACE)
                .keyDown(Keys.SHIFT)
                .sendKeys("a")
                .keyUp(Keys.SHIFT)
                .sendKeys("71")
                .sendKeys(Keys.ENTER)
                .perform();

        // 3- aramanin gerceklestigini test edin
        String expectedResult ="\"Samsung A71\"";
        String actualRestul =driver.findElement(By.xpath("//span[text()='\"Samsung A71\"']")).getText();
        Assert.assertEquals(actualRestul,expectedResult,"SearchResult Test");


    }

    @AfterMethod
    public void tearDown() {
        //4- sayfayı kapatın
        driver.close();
    }


}
