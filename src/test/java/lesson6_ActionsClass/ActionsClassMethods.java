package lesson6_ActionsClass;

public class ActionsClassMethods {

    /**
     * Actions Class klavye ve mouse ile yapılacak her seyı yapabılme ımkanını sunar.
     * Actions class method'ları, klavye ve mouse olmak uzere ıkıye ayrılır.
     * Method kullanımından sonra .perfom() ıle bıtırılmesı gerekmektedır.
     *
     * 1-Mouse Methodları:
     *   - actions.moveToElement(variable) : Bir webelementin uzerıne gıdıp beklememızı saglar.
     *   - actions.moveToElement(variable, X,Y): Webelementın herhangı bır yerınde durmak icin webelement ve piksel girilir.
     *   - actions.click(): Mouse'un bulundugu yere tıklar. (herhangı bır yer)
     *   - actions.click(variable): Bir webelemente click yapar.
     *   - actions.clickAndHold(): Mouse'un bulundugu yerde tıkla ve tut yapar. (herhangı bır yer)
     *   - actions.clickAndHold(variable): 'Tıkla ve tut' islemi yapar. Webelement uzerınde.
     *   - actions.contextClick(): Mouse'un bulundugu yerde sag click yapar. (herhangı bır yer)
     *   - actions.contextClick(variable): Bir webelemenete sag click yapar.
     *   - actions.doubleClick(): Mouse'un bulundugu yerde cıft tıklama (sol) islemi yapar. (herhangı bır yer)
     *   - actions.doubleClick(variable): Bir webelemente (sol) cift tıklama yapar.
     *   - actions.dragAndDrop(variable1, variable2): Bir webelementi alır diger bir webelementın ustune bırakır.
     *   - actions.dragAndDrop(variable, X,Y): Bir webelementi alır ıstenılen pixele bırakır
     *
     * 2-Klavye methodları:
     *   Temel olarak iki hareket vardır.
     *   A- Istedigimiz tusa tek seferlık basmak
     *      - actions.sendKeys(Keys.tus): Istenilen herhangı bır klavye tusuna bır defa basar.
     *   B- Istedigimiz tusa istedigimiz sure basma
     *      - actions.keyDown(Keys.tus): Tusa basar ve bekler, bırakmaz.
     *      - actions.keyUp(Keys.tus): Ara ıslemler gectıkten sonra (istenilen sure gectıkten sonra) basılı olan tusu bırakır.
     */

    /**
     * Actions method kullanımı:
     * 1- Actions class'ından obje uretilir --> Actions actions = new Actions(driver);
     * 2- Actions ile kullanılacak olan webelementi locate edilir.
     * 3- Istenilen methodlar actions objesi ile kullanılır;
     *    actions.click(variable)
     *           .sendKeys("Ahmet")
     *           .sendKeys(Keys.TAB)
     *           .sendKeys("as@asdf.com")
     *           .sendKeys(Keys.TAB)
     *           .sendKeys(Keys.ENTER)
     *           .perform();
     *
     * Not: Actions ile baslayan bir kodun son parcası .perform() ıle bıtmek zorundadır.
     *      Compiler hatası vermese de ıstenılenı yapmaz.
     */
}
