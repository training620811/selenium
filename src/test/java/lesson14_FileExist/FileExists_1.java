package lesson14_FileExist;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.nio.file.Files;
import java.nio.file.Paths;

public class FileExists_1 {

    public static void main(String[] args) {

        // Masaustune Test klasoru ıcerınse deneme.txt isminde bir dosya olusturun
        // Daha sonra bu dosyanın bılgısayarınızda var olup olmadıgını test edin.


        // C:\Users\ACER-ULTRABOOK\Desktop\Test\deneme.txt


        System.out.println(System.getProperty("user.home")); // C:\Users\ACER-ULTRABOOK
    }
    @Test
    public void test(){
        String dosyaYolu = System.getProperty("user.home")+"\\Desktop\\Test\\deneme.txt";
        Boolean result = Files.exists(Paths.get(dosyaYolu));
        Assert.assertTrue(result);
    }

}
