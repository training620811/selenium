package lesson14_FileExist;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.nio.file.Files;
import java.nio.file.Paths;

public class FileExist_2 {

    // Projenizde pom.xml oldugunu test edin.

    // System.out.println(System.getProperty("user.dir")); // C:\JavaProjects\TrainingWithSelenium

    //C:\JavaProjects\TrainingWithSelenium\pom.xml

    @Test
    public void test(){
        String dosyaYolu = System.getProperty("user.dir")+"\\pom.xml";
        Boolean result = Files.exists(Paths.get(dosyaYolu));
        Assert.assertTrue(result);
    }
}
