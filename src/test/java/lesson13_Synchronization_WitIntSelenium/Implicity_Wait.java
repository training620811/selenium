package lesson13_Synchronization_WitIntSelenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class Implicity_Wait {
    /**
     * 1- https://www.amazon.com/ gidin
     * 2- "iphone" aratın.
     */

    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));

        driver.get("https://www.amazon.com/");

        WebElement searchElement = driver.findElement(By.id("twotabsearchtextbox"));

        searchElement.sendKeys("iphone", Keys.ENTER);
    }

}
