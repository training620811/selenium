package lesson11_BasicAuthentication;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class explanation {
    /**
     * Basıc authentication : Basit kımlık dogrulama
     * Karsımıza bır alert cıkar. Bu alert'e  Js ıle ve normal yolla da erısemeyız.
     * Otomatık test ıle yapabılmek ıcın ılgılı developer'dan logın olmak ıcın URL talep edilir.
     * HTML komutu : https://username:password@URL
     *
     * URL : Ilgili sayfa, username ve password ıse kısıye aıt kullanıcı adı ve sıfre.
     *       Application a aıt URL developerdan ıstenır.
     */

    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        WebDriver driver =new ChromeDriver();
        driver.manage().window().maximize();

        //driver.get("https://the-internet.herokuapp.com/basic_auth");
        driver.get("https://admin:admin@the-internet.herokuapp.com/basic_auth");

        // HTML komutu : https://username:password@URL

    }
}

