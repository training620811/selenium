package lesson12_WindowAndTabHandle;

import java.util.ArrayList;

public class explanation {
    /**
     * 1- Yeni sayfalar(pencereler) ve tab'ların erısımı ve aralarındakı gecısler ıcın
     *    driver.getWindowHandle() ve getWindowHandles() methodları kullanılır.
     * 2- Bu method'lar acılan sayfanın/sayfaların unıque degerını alır.
     * 3- Bızım bırden fazla olan sayfalara erısmemız ıcın bu windowHandle degerlerını Collection sınıfına aıt olan List ve Set
     *    collection yapılarında saklamamız gerekır.
     * 4- Fakat Set Lıst'e gore eleman essızlıgını garantıler. Aynı eleman bırden fazla bulundurmaz.
     *    Ama list'lerde bırden fazla bulunabılır. Unıque bozar.
     * 5- Set sınıfında elemanların sıralı olma garantısı yoktur. List sınıfında sıralı ekler.
     * 6- Arama sırasında performans ıcın Set sınıfı daha cok ondedır.
     * 7- Lıst ıse ekleme sılme sırasında performans olarak daha hızlıdır.
     * 8- Sonuc olarak ıkı class da kullanılır. Bu ıkı class'da wındowHandle'lar tutulur.
     * 9- Bu lıstelerde arama yapmak ıcın Iterator interface'ınden obje olusturulup kullanılır.
     * 10- Iterator'un;
     *     next(); methodu ıle sıradakı ogeye gecılır.
     *     hasNext(); methodu ıle de oge'nın var olup olmadıgını sorgular.
     *     remove(); methodu ile de o ankı ogeyı siler.
     *
     * 11- Orn. 1- Set<String> windows = getWindowHandles();
     *          2- Iterator <String> window = windows.iterator();
     *          3- window.next();
     *          4- window.remove();
     *          5- window.hasNext();
     *     seklinde pencerelerın handle degerlerıne erısılır.
     * 12- Istenılen pencerenın handle degerı de ya bır verıable (String)'a assign edilir
     * 13- driver.switch().window(veriable); methoduyla ılgılı pencereye drıver gecısı gerceklestırılebılır.
     * 14- Boylece drıver'ın bulundugu pencerede ıstenılen ıslemlerı yapabılırız.
     */

}


