package lesson12_WindowAndTabHandle;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.time.Duration;
import java.util.Iterator;
import java.util.Set;

public class TestCase1 {
    /**
     * 1- https://the-internet.herokuapp.com/windows URL gidin
     * 2- Click Here butonuna basın.
     * 3- Acilan yeni pencerenin sayfa başlığının (title) “New Window” oldugunu dogrulayin.
     * 4- Bir önceki pencereye geri döndükten sonra sayfa başlığının “The Internet” olduğunu doğrulayın.
     * 5- Elemental Selenium butonuna tıklayın
     * 6- Acilan yeni pencerenin sayfa başlığının (title) “Elemental Selenium” icerdigini dogrulayin.
     * 7- Bir önceki pencereye geri döndükten sonra sayfa başlığının “The Internet” olduğunu doğrulayın.
     * 8- Browser'i kapatın
     */

    static WebDriver driver;
    static SoftAssert softAssert;

    //TITLE
    static String expectedChildOneTitle;
    static String actualChildOneTitle;
    static String expectedParentTitle;
    static String actualParentTitle;
    static String expectedChildTwoTitle;
    static String actualChildTwoTitle;


    //URL
    static String parentURL = "https://the-internet.herokuapp.com/windows";

    //Windows
    static String parentHandle;
    static String childOneHandle;
    static String childTwoHandle;
    static Set<String> windows;
    static Iterator<String> window;

    @BeforeClass
    public static void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
        softAssert = new SoftAssert();

        //1
        driver.get(parentURL);

    }

    @AfterClass
    public static void tearDown() {
        //8
        driver.quit();
    }

    @Test
    public void test01() {
        //2
        WebElement clickHereButton = driver.findElement(By.xpath("//a[text()='Click Here']"));
        clickHereButton.click();
    }

    @Test
    public void test02() {
        //3
        windows = driver.getWindowHandles();
        window = windows.iterator();
        parentHandle = window.next();
        childOneHandle = window.next();
        driver.switchTo().window(childOneHandle);

        System.out.println();
        expectedChildOneTitle = "New Window";
        actualChildOneTitle = driver.getTitle();
        softAssert.assertEquals(actualChildOneTitle, expectedChildOneTitle);

    }

    @Test
    public void test03() {
        //4
        driver.switchTo().window(parentHandle);
        expectedParentTitle = "The Internet";
        actualParentTitle = driver.getTitle();
        softAssert.assertEquals(actualParentTitle, expectedParentTitle);

    }

    @Test
    public void test04() {
        //5
        WebElement elementSeleniumButton = driver.findElement(By.xpath("//a[text()='Elemental Selenium']"));
        elementSeleniumButton.click();
    }

    @Test
    public void test05() {
        //6
        windows = driver.getWindowHandles();
        window = windows.iterator();
        parentHandle = window.next();
        childOneHandle = window.next();
        childTwoHandle = window.next();
        System.out.println();
        driver.switchTo().window(childTwoHandle);

        expectedChildTwoTitle = "Elemental Selenium: Receive a Free, Weekly Tip on Using Selenium like a Pro";
        actualChildTwoTitle = driver.getTitle();
        softAssert.assertEquals(actualChildTwoTitle, expectedChildTwoTitle);
    }

    @Test
    public void test06(){
        //7
        driver.switchTo().window(parentHandle);
        actualParentTitle = driver.getTitle();
        softAssert.assertEquals(actualParentTitle,expectedParentTitle);
    }
    @Test
    public void test07(){
        softAssert.assertAll();
    }
}
