package lesson4_JUnitFramework;

public class Explanation {

    /**
     * Junit, Java programlama dili icin ünit test framework'u olarak bilinir.
     * Unit test, bir yazılımın en kucuk test edılebılır parcasının, tek basına ve bagımsız olarak test edılmesıdır.
     * Framework, uzerinde yazılım olusturabılecegımız bır yapıdır.
     */

    /**
     * JUnit Framework avantajları:
     * 1- Acık kaynaklı
     * 2- Test methodlarını bagımsız tanımlamak ve calıstırmak ıcın notasyonlar(Annotations) saglar. Main method ihtiyacı biter.
     * 3- ExpectedResult ve ActualResult'ı karsılastırarak otomatık test etmemızı saglayan hazır assertion method'ları barındırır.
     *    Sonucları bıze otomatık doner. Boylece if-else block'ları ve sonucları yazdırma gereksınımı kalmaz.
     * 4- JUnit framework kullanabılmek ıcın maven ile olusturulan projenın pom.xml dosyasına mvn.com'dan JUnit dependency eklenmelidir.
     * 5- JUnit'in sagladıgı notasyonlar veriable, method, class, package icin kullanılabılır.
     * 6- Notasyonlar (Annotations) ile compiler(derleyici)'a talimatlar veririz. (Su testı once yap, su ıkısını yapma vs gibi.)
     * 7- En cok kullanılan Annotation'lar;
     *    - @Test : Method'ları main method olmadan bagımsız calıstırabılmeyı saglar. Bir class'da birden fazla Test annotation olabılır.
     *              Class sevıyesınde calıstırılırsa sırasıyla Test annotation'ları calıstırır.
     *    - @BeforeClass @AfterClass :
     *    - @Before, @After :
     *    - @Ignore
     */
}
