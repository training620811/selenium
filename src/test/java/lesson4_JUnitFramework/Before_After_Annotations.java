package lesson4_JUnitFramework;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class Before_After_Annotations {
    /**
     * Before annotation bir Test annotation'undan once calısır. Yani Test annotation calısmaya basladıgında class'a bakar
     * eger Before annotation varsa once onu calıstırır.
     *
     * After annotation bir Test annotation'undan sonra calısır. Yani Test annotation calısmaya basladıgında class'a bakar
     * eger After annotation varsa Test annotation'dan sonra onu calıstırır.
     *
     * Not: Her Test annotation'dan once ve sonra Before/After annotationsları calısır.
     */
    WebDriver driver;
    @Before
    public void setup(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
    }

    @After
    public void tearDown(){
        driver.close();
    }

    @Test
    public void test01(){
        driver.get("https://www.amazon.com");
    }

    @Test
    public void test02(){
        driver.get("https://www.bestbuy.com");
    }

    @Test
    public void test03(){
        driver.get("https://www.facebook.com");
    }
}
