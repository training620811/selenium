package lesson4_JUnitFramework;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class Ignore_Annotation {
    /**
     * Ignore annotation, bu notasyona ait olan methodu ignore eder yanı gormezden gelir. Herhangı bir test icin hazırlanan
     * method'un henuz kod calısmaları bıtmemıs olabilir, genelde boyle zamanlarda kullanılabılır.
     */

    static WebDriver driver;
    @BeforeClass
    public static void setup(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
    }

    @AfterClass
    public static void tearDown(){
        driver.close();
    }

    @Test
    public void test01(){
        //Amazona gidelim
        driver.get("https://www.amazon.com");
    }

    @Test @Ignore
    public void test02(){
        //Title yazdıralım
        System.out.println(driver.getTitle());

    }

    @Test
    public void test03(){
        System.out.println(driver.getCurrentUrl());
    }
}
