package lesson4_JUnitFramework;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class BeforeClass_AfterClass_Annotations {

    /**
     * Olusturdugumuz test class'ının ıcındekı Test methodları hep aynı web sıtesı ıle ılgili ise her seferınde yeniden
     * driver olusturmaya ve her method ıcın bu drıver'ı kapatmaya ıhtıyac yoktur.
     *
     * Eger class'ın basında bır defa setup calısıp en son da bır defa kapansın dersek BeforeClass ve AfterClass kullanırız.
     * Bu durum da kullanıslı olacaktır.
     *
     * Not :BeforeClass ve AfterClass method'ları static olmak zorundadır.
     *      Boylece methodların dısındakı verıable'lar da static olarak declare edilmelidir.
     */

    static WebDriver driver;
    @BeforeClass
    public static void setup(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
    }

    @AfterClass
    public static void tearDown(){
        driver.close();
    }

    @Test
    public void test01(){
        //Amazona gidelim
        driver.get("https://www.amazon.com");
    }

    @Test
    public void test02(){
        //Title yazdıralım
        System.out.println(driver.getTitle());

    }

    @Test
    public void test03(){
        System.out.println(driver.getCurrentUrl());
    }
}
