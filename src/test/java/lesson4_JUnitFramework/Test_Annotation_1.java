package lesson4_JUnitFramework;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class Test_Annotation_1 {

    /**
     * Herhangı bır method'a Test annotation eklenirse main methoda gerek kalmaz.
     */

    @Test
    public void test01(){
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));


        // 1- https://www.amazon.com sayfasına gidin.
        driver.get("https://www.amazon.com");

        // 2- Arama cubuguna "Nutella" yazdırın
        WebElement aramaKutusu = driver.findElement(By.id("twotabsearchtextbox"));
        aramaKutusu.sendKeys("Nutella");



        // 3- Nutella yazdıktan sonra ENTER'a basarak arama ıslemı yapın.
        aramaKutusu.submit();

        // 4- Bulunan sonuc sayısını yazdırın.
        WebElement resultSearchElement = driver.findElement(By.xpath("//div[@class ='a-section a-spacing-small a-spacing-top-small']"));
        System.out.println("Result : " + resultSearchElement.getText());

        // 5- Sayfayı kapatın.
        driver.close();

    }


}
