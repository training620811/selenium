package lesson5_JUnitAssertions.explanation;

import org.junit.Assert;
import org.junit.Test;

public class Test3_WithAssertionMethods {
    int a = 50;
    int b = 60;
    int c = 60;
    int d = 70;
    String isim1 = "Ali";
    String isim2 = "Ali";


    @Test
    public void test01() {
        // a>b
        Assert.assertTrue(a > b); // Test FAILED
    }

    @Test
    public void test02() {
        // d>b
        Assert.assertTrue(d > b); // Test PASSED
    }

    @Test
    public void test03() {
        // a>b
        Assert.assertFalse(a > b); // Test PASSED
    }

    @Test
    public void test04() {
        // d>c
        Assert.assertFalse(d > c); // Test FAILED
    }

    @Test
    public void test05() {
        // d=c
        Assert.assertEquals(d, c); // Test FAILED
    }

    @Test
    public void test06() {
        // b,c
        Assert.assertEquals(b, c); // Test PASSED
    }

    @Test
    public void test07() {
        // isim1=isim2
        Assert.assertEquals(isim1, isim2); // Test PASSED
    }

    @Test
    public void test08() {
        // isim1= "ali"
        Assert.assertEquals(isim1, "ali"); // Test FAILED
    }

    @Test
    public void test09() {
        // isim1="ALI"
        Assert.assertEquals(isim1, "ALI"); // Test FAILED
    }

}
