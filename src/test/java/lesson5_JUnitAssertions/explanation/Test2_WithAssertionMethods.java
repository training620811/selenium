package lesson5_JUnitAssertions.explanation;

import org.junit.Assert;
import org.junit.Test;

public class Test2_WithAssertionMethods {

    int aliYas = 56;
    int veliYas = 68;
    int emeklilikYas = 65;

    //1- Veli'nin emekli OLABILECEGINI test edin.
    @Test
    public void test01(){
        Assert.assertTrue(veliYas > emeklilikYas);
    }

    //2- Ali'nin emekli OLAMAYACAGINI test edin.
    @Test
    public void test02(){
        Assert.assertFalse(aliYas > emeklilikYas);
    }

    //3- Emekli yasının 65 OLDUGUNU test edin.
    @Test
    public void test03(){
        Assert.assertEquals(65,emeklilikYas);
    }

    //4- Ali'nin emekli OLABILECEGINI test edin.
    @Test
    public void test04(){
        Assert.assertTrue(aliYas>emeklilikYas);
    }

    //5- Veli'nin emekli OLAMAYACAGINI test edin
    @Test
    public void test05(){
        Assert.assertFalse(veliYas>emeklilikYas);
    }

    //6- Emeklilik yasının 63 OLDUGUNU test edin.
    @Test
    public void test06(){
        Assert.assertEquals(63,emeklilikYas);
    }
}
