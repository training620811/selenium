package lesson5_JUnitAssertions.explanation;

public class ExplanationsAndAssertionsMethods {

    /**
     * Assertions, actual ve expected karsılastırarak test etmemızı saglar.
     * İf-Else kullanmak ve Passed/Failed yazdırmak zorunda olmayız.
     *
     * JUnit, kodları calıstırırken bastan sona bır sorunla karsılasıp karsılasmadıgına bakar.
     * Yani sorunla karsılasırsa Test FAILED code -1, karsılasmazsa Test PASSED code 0 verir.
     *
     * Dogru raporlama dogru sonuc ıcın Assertion Class'ını JUnıt bızlere saglar.
     *
     * JUnit assertion(verify/dogrulama) icin Assertion class'ından statıc methodlar kullanılır.
     *
     */

    /**
     * 1- Assert.assertTrue(KOSUL) : Dogru olmasını ıstedıgımız kosullarda kullanılır.   (Orn: Logın'de girilmesini istersek)
     * 2- Assert.assertFalse(KOSUL) : Yanlıs olmasını ıstedıgımız kosullarda kullanılır. (Orn: Login'de gırılmemesını ıstersek)
     * 3- Assert.assertEqual(Expected,actual) : Ikı deger verıp o ıkı degerın bırbırıne esıt olmasını beklıyorsak kullanılır.
     */


}
