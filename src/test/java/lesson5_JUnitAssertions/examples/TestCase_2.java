package lesson5_JUnitAssertions.examples;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class TestCase_2 {

    /**
     * 1. Launch browser
     * 2. Navigate to url 'http://automationexercise.com'
     * 3. Verify that home page is visible successfully
     * 4. Click on 'Products' button
     * 5. Verify user is navigated to ALL PRODUCTS page successfully
     * 6. Enter product name in search input and click search button
     * 7. Verify 'SEARCHED PRODUCTS' is visible
     * 8. close driver
     */

    WebDriver driver;

    @Before
    public void setup() {
        //1. Launch browser
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @After
    public void tearDown() {
        //8. close driver
        driver.close();
    }

    @Test
    public void test() {
        //2. Navigate to url 'http://automationexercise.com'
        driver.get("http://automationexercise.com");

        //3. Verify that home page is visible successfully
        WebElement logoElement = driver.findElement(By.xpath("//img[@src='/static/images/home/logo.png']"));
        Assert.assertTrue(logoElement.isDisplayed());

        //4. Click on 'Products' button
        driver.findElement(By.xpath("//a[text()=' Products']")).click();

        //5. Verify user is navigated to ALL PRODUCTS page successfully
        WebElement allProductYazisi = driver.findElement(By.xpath("//h2[text()=\"All Products\"]"));
        Assert.assertTrue(allProductYazisi.isDisplayed());

        //6. Enter product name in search input and click search button
        WebElement searchKutusu = driver.findElement(By.xpath("//input[@id='search_product']"));
        searchKutusu.sendKeys("tshirt");
        driver.findElement(By.xpath("//button[@id='submit_search']")).click();

        //7. Verify 'SEARCHED PRODUCTS' is visible
        WebElement searchProductTextElementi =driver.findElement(By.xpath("//h2[text()='Searched Products']"));
        Assert.assertTrue(searchProductTextElementi.isDisplayed());

    }
}
