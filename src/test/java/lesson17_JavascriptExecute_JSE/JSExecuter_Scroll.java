package lesson17_JavascriptExecute_JSE;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import java.time.Duration;

public class JSExecuter_Scroll {
    public static void main(String[] args) throws InterruptedException {

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));

        driver.get("https://www.amazon.com/");


        JavascriptExecutor jsx = (JavascriptExecutor) driver;

        WebElement element = driver.findElement(By.xpath("//a[text()='Sell products on Amazon']"));

        jsx.executeScript("arguments[0].scrollIntoView(true)",element);
        Thread.sleep(3000);
        jsx.executeScript("arguments[0].click()",element);

        String expectedURL = "https://sell.amazon.com/?ref_=asus_soa_rd&ld=AZFSSOA&ref_=footer_soa";
        String actualURL = driver.getCurrentUrl();

        Assert.assertEquals(actualURL,expectedURL);

        driver.close();


    }
}
