package lesson15_FileDownload_FileUpload;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;

public class FileDownload {

    /**
     * https://the-internet.herokuapp.com/download URL'e gidin
     * archivo.docx dosyasını indirin
     * archivo.docx dosyasının indirildigini kontrol edin
     * Browser'i kapatin
     */

    static WebDriver driver;

    @BeforeClass
    public static void setup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
    }

    @Test
    public void test() throws InterruptedException {
        driver.get("https://the-internet.herokuapp.com/download");

        Thread.sleep(2000);
        WebElement link = driver.findElement(By.xpath("//a[text()='archivo.docx']"));
        link.click();
        Thread.sleep(2000);


        // C:\Users\ACER-ULTRABOOK\Downloads\archivo.docx
        String dosyaYolu = System.getProperty("user.home") + "\\Downloads\\archivo.docx";
        Boolean isFileExist = Files.exists(Paths.get(dosyaYolu));
        Assert.assertTrue(isFileExist);
    }


}
