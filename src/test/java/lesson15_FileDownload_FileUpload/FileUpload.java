package lesson15_FileDownload_FileUpload;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.checkerframework.checker.units.qual.C;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.Duration;

public class FileUpload {
    /**
     * https://the-internet.herokuapp.com/upload adresine gidelim
     * Dosya Sec butonuna basalim
     * Yuklemek istediginiz dosyayi secelim.
     * Upload butonuna basalim.
     * “File Uploaded!” textinin goruntulendigini test edelim
     */

    static WebDriver driver;
    @BeforeClass
    public static void setup(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

    }

    @Test
    public void test(){
        driver.get("https://the-internet.herokuapp.com/upload");

        WebElement dosyaSecButton = driver.findElement(By.id("file-upload"));

        String dosyaYolu ="C:\\Users\\ACER-ULTRABOOK\\Downloads\\archivo.docx";

        dosyaSecButton.sendKeys(dosyaYolu);

        WebElement uploadButton = driver.findElement(By.id("file-submit"));
        uploadButton.click();

        WebElement fileUploadText = driver.findElement(By.xpath("//h3[.='File Uploaded!']"));
        Boolean isDisplayedText =fileUploadText.isDisplayed();
        Assert.assertTrue(isDisplayedText);

    }
}
