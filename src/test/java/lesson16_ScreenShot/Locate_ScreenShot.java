package lesson16_ScreenShot;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

public class Locate_ScreenShot {

    public static void main(String[] args) throws IOException {

        /**
         * Amazon'a gıt
         * Arama kutusuna apple yaz ve ara
         * Arama sonucunun ekran goruntusunu al
         * Driver'ı kapat
         */

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));

        //1
        driver.get("https://WWW.amazon.com");

        //2
        WebElement searchBox = driver.findElement(By.id("twotabsearchtextbox"));
        searchBox.sendKeys("Apple", Keys.ENTER);

        //3

        WebElement webElement = driver.findElement(By.xpath("(//div[@class='a-section a-spacing-small a-spacing-top-small'])[1]"));

        File locateSS = new File ("target/screenshot/locateSS.png");

        File geciciResim = webElement.getScreenshotAs(OutputType.FILE);

        FileUtils.copyFile(geciciResim,locateSS);

        //4
        driver.close();




    }
}
