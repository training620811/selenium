package lesson16_ScreenShot;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

public class FullPAge_ScreenShot {

    public static void main(String[] args) throws IOException {
        /**
         * Amazon'a gıt
         * Arama kutusuna apple yaz ve ara
         * Tum sayfanın ekran goruntusunu al
         * Driver'ı kapat
         */

        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));

        //1
        driver.get("https://WWW.amazon.com");

        //2
        WebElement searchBox = driver.findElement(By.id("twotabsearchtextbox"));
        searchBox.sendKeys("Apple", Keys.ENTER);

        //3
        TakesScreenshot tss = (TakesScreenshot) driver;

        File tumSayfaSS = new File("target//screenshot//tumSayfaSS.png");

        File geciciResim = tss.getScreenshotAs(OutputType.FILE);

        FileUtils.copyFile(geciciResim,tumSayfaSS);


        //4
        driver.close();

    }
}
